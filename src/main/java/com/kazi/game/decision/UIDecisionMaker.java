package com.kazi.game.decision;

import com.kazi.game.model.Move;

import java.util.Arrays;
import java.util.List;

public class UIDecisionMaker implements DecisionMaker {

    private static final List<String> EXIT_WORDS = Arrays.asList("QUIT", "EXIT");
    private static final List<String> INVENTORY_WORDS = Arrays.asList("INVE", "INVENTORY");

    @Override
    public Move getMove(String command) {
        if (EXIT_WORDS.contains(command.toUpperCase())) {
            return new Move(Move.MoveType.QUIT);
        } else if (command.toUpperCase().startsWith("GET")) {
            return new Move(Move.MoveType.ARTIFACT_ADD, command.substring(4));
        } else if (command.toUpperCase().startsWith("DROP")) {
            return new Move(Move.MoveType.ARTIFACT_DROP, command.substring(5));
        } else if (command.toUpperCase().startsWith("USE")) {
            return new Move(Move.MoveType.ARTIFACT_USE, command.substring(4));
        } else if (command.toUpperCase().startsWith("LOOK")) {
            return new Move(Move.MoveType.LOOK);
        } else if (INVENTORY_WORDS.contains(command.toUpperCase())) {
            return new Move(Move.MoveType.INVENTORY);
        } else if (command.toUpperCase().startsWith("MOVE")) {
            return new Move(Move.MoveType.MOVE, command.substring(5));
        } else if (command.toUpperCase().startsWith("WAVE")) {
            return new Move(Move.MoveType.WAVE_HAND);
        } else if (command.toUpperCase().startsWith("TEXT")) {
            return new Move(Move.MoveType.TEXT);
        } else if (command.toUpperCase().startsWith("GUI")) {
            return new Move(Move.MoveType.GUI, command.substring(4));
        } else {
            return new Move(Move.MoveType.NONE);
        }
    }
}
