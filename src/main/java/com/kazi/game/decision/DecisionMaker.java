package com.kazi.game.decision;

import com.kazi.game.model.Move;

public interface DecisionMaker {
    Move getMove(String command);
}
