package com.kazi.game.decision;

import com.kazi.game.model.Move;

public class AIDecisionMaker implements DecisionMaker {
    @Override
    public Move getMove(String command) {
        if (command.toUpperCase().startsWith("GET")) {
            return new Move(Move.MoveType.ARTIFACT_ADD, command.substring(4));
        } else if (command.toUpperCase().startsWith("DROP")) {
            return new Move(Move.MoveType.ARTIFACT_DROP, command.substring(5));
        } else if (command.toUpperCase().startsWith("USE")) {
            return new Move(Move.MoveType.ARTIFACT_USE, command.substring(4));
        } else if (command.toUpperCase().startsWith("MOVE")) {
            return new Move(Move.MoveType.MOVE, command);
        } else if (command.toUpperCase().startsWith("TALK")) {
            return new Move(Move.MoveType.TALK, command);
        } else {
            return new Move(Move.MoveType.NONE);
        }
    }
}
