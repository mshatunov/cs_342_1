package com.kazi.game;

import com.kazi.game.model.*;
import com.kazi.game.model.artifact.ArtifactFactory;
import com.kazi.game.model.character.CharacterFactory;
import com.kazi.game.model.place.PlaceFactory;

import java.io.File;
import java.util.Optional;
import java.util.Scanner;

public class GameTester {

    public static final String DEFAULT_PATH = "src/main/resources/game.gdf";

    public static void main(String[] args) {
        Game game = new Game("New game");
        String gdfPath = Optional.ofNullable(args)
                .filter(a -> a.length > 0)
                .map(a -> a[0]).orElse(DEFAULT_PATH);
        initGameFromGDF(gdfPath);
        game.play();
    }

    private static void initGameFromGDF(String arg) {
        File file = new File(arg);
        try (Scanner scanner = new Scanner(file)) {
            scanner.next();
            double gdfVersion = scanner.nextDouble();
            System.out.println(gdfVersion);
            System.out.println("----------------");

            if (scanner.next().equals("PLACES")){
                int placesIterations = scanner.nextInt();
                System.out.println("Creating " + placesIterations + " places");
                for (int i = 0; i < placesIterations; i++) {
                    String placeType;
                    if (gdfVersion > 5) {
                        placeType = scanner.next();
                    } else {
                        placeType = "BASIC";
                    }
                    PlaceFactory.createPlace(scanner, placeType);
                }
                System.out.println("----------------");
            }

            if (scanner.next().equals("DIRECTIONS")) {
                int directionsIterations = scanner.nextInt();
                System.out.println("Creating " + directionsIterations + " directions");
                for (int i = 0; i < directionsIterations; i++) {
                    new Direction(scanner);
                }
                System.out.println("----------------");
            }

            if (scanner.next().equals("CHARACTERS")) {
                int characterIterations = scanner.nextInt();
                System.out.println("Creating " + characterIterations + " characters");
                for (int i = 0; i < characterIterations; i++) {
                    CharacterFactory.createCharacter(scanner);
                }
                System.out.println("----------------");
            }

            if (scanner.next().equals("ARTIFACTS")) {
                int artifactsIterations = scanner.nextInt();
                System.out.println("Creating " + artifactsIterations + " artifacts");
                for (int i = 0; i < artifactsIterations; i++) {
                    String artifactType;
                    if (gdfVersion > 5) {
                        artifactType = scanner.next();
                    } else {
                        artifactType = "BASIC";
                    }
                    ArtifactFactory.createArtifact(scanner, artifactType);
                }
                System.out.println("----------------");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
