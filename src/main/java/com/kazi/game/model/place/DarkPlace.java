package com.kazi.game.model.place;

import com.kazi.game.io.IO;
import com.kazi.game.model.artifact.Artifact;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class DarkPlace extends Place {
    public DarkPlace(Scanner scanner) {
        super(scanner);
    }

    @Override
    public List<Artifact> getArtifacts() {
        IO.getCurrentInterface().display("There are no artifacts, because it's so dark and you cant't see them");
        return Collections.emptyList();
    }
}
