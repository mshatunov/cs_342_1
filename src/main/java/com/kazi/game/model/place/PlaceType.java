package com.kazi.game.model.place;

import java.util.stream.Stream;

public enum PlaceType {
    BASIC("BASIC"),
    MAGICAL("MAGICAL"),
    DARK("DARK"),
    MAGNETIC("MAGNETIC");

    PlaceType(String type) {
        this.type = type;
    }

    private String type;

    public String getType() {
        return type;
    }

    public static PlaceType getPlaceType(String inputType) {
        return Stream.of(PlaceType.values())
                .filter(t -> t.getType().equalsIgnoreCase(inputType))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No such place type"));

    }
}
