package com.kazi.game.model.place;

import com.kazi.game.io.IO;
import com.kazi.game.model.artifact.Artifact;
import com.kazi.game.model.Direction;

import java.util.*;

import static com.kazi.game.model.Direction.valueOfDirType;

public class Place {
    public final static Map<Integer, Place> places = new HashMap<>();
    protected final int id;
    protected final String name;
    protected final String description;
    protected final List<Direction> directions = new ArrayList<>();
    protected final List<Artifact> artifacts = new ArrayList<>();

    public Place(Scanner scanner) {
        int id = scanner.nextInt();
        String name = scanner.nextLine().trim();
        int linesOfDescription = Integer.parseInt(scanner.nextLine());
        String description = "";
        for (int i = 0; i < linesOfDescription; i++) {
            description += scanner.nextLine();
        }
        this.id = id;
        this.name = name;
        this.description = description;
        places.put(id, this);
        IO.getCurrentInterface().display("Created place:" + this);
    }

    public static Place getPlaceById(int id) {
        return places.get(id);
    }

    public String name() {
        return name;
    }

    public List<Direction> getDirections() {
        return directions;
    }

    public void addDirection(Direction direction) {
        directions.add(direction);
    }

    public Place followDirection(String direction) {
        return directions.stream()
                .filter(d -> valueOfDirType(direction) == (d.getDir()))
                .filter(d -> !d.isLocked())
                .findFirst()
                .map(Direction::follow)
                .orElse(this);
    }
    
    public void display() {
        IO.getCurrentInterface().display(this.toString());
    }

    @Override
    public String toString() {
        return "Place{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", artifacts=" + artifacts +
                '}';
    }

    public List<Artifact> getArtifacts() {
        return artifacts;
    }

    public void addArtifact(Artifact artifact) {
        artifacts.add(artifact);
    }

    public void useKey(Artifact artifact) {
        directions.forEach(d -> d.useKey(artifact));
    }
}
