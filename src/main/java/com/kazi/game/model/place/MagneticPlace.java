package com.kazi.game.model.place;

import com.kazi.game.io.IO;
import com.kazi.game.model.artifact.Artifact;

import java.util.Scanner;

public class MagneticPlace extends Place {
    public MagneticPlace(Scanner scanner) {
        super(scanner);
    }

    @Override
    public void addArtifact(Artifact artifact) {
        IO.getCurrentInterface().display("It's a magnetic place, your artifact is lost forever ;=)");
    }
}
