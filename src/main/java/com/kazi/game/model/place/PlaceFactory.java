package com.kazi.game.model.place;

import java.util.Scanner;

public class PlaceFactory {
    public static Place createPlace(Scanner scanner, String placeType) {
        switch (PlaceType.getPlaceType(placeType)) {
            case BASIC:
                return new BasicPlace(scanner);
            case MAGICAL:
                return new MagicalPlace(scanner);
            case DARK:
                return new DarkPlace(scanner);
            case MAGNETIC:
                return new MagneticPlace(scanner);
            default:
                throw new IllegalStateException("No such place type");
        }
    }
}
