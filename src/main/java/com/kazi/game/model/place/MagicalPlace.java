package com.kazi.game.model.place;

import com.kazi.game.io.IO;
import com.kazi.game.model.artifact.Artifact;
import com.kazi.game.model.Direction;

import java.util.Scanner;

public class MagicalPlace extends Place {
    public MagicalPlace(Scanner scanner) {
        super(scanner);
    }

    @Override
    public void useKey(Artifact artifact) {
        IO.getCurrentInterface().display("It's a magical place, all directions are unlocked ;=)");
        directions.forEach(Direction::unlock);
    }
}
