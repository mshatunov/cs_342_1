package com.kazi.game.model.artifact;

import com.kazi.game.io.IO;
import com.kazi.game.model.character.Character;
import com.kazi.game.model.place.Place;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public abstract class Artifact {
    public static final Map<String, Artifact> artifacts = new HashMap<>();
    private int id;
    private String name;
    private String description;
    private String value;
    private int mobility;
    private int keyPattern;

    public Artifact(Scanner scanner) {
        int id = scanner.nextInt();
        IO.getCurrentInterface().display(id);
        if (id < 0) {
            id *= (-1);
            Character.getCharacterByID(id).getArtifacts().add(this);
            IO.getCurrentInterface().display("Artifact given to " + Character.getCharacterByID(id).getName());
        } else if (id == 0) {
            Random rand = new Random();
            Object[] array = Place.places.values().toArray();
            Place randomPlace = (Place) array[rand.nextInt(array.length)];
            randomPlace.addArtifact(this);
            IO.getCurrentInterface().display("Artifact placed in " + randomPlace.name());
        } else {
            Place.getPlaceById(id).addArtifact(this);
            IO.getCurrentInterface().display("Artifact placed in " + Place.getPlaceById(id).name());
        }

        this.id = scanner.nextInt();
        this.value = scanner.next();
        this.mobility = scanner.nextInt();
        this.keyPattern = scanner.nextInt();
        this.name = scanner.next();

        scanner.nextLine();
        int linesOfDescription = scanner.nextInt();
        scanner.nextLine();

        String description = "";
        for (int i = 0; i < linesOfDescription; i++) {
            description += scanner.nextLine();
        }
        this.description = description;
        artifacts.put(this.name.toLowerCase(), this);
        IO.getCurrentInterface().display("Created artifact:" + this);
    }

    public static Artifact getArtifactByName(String name) {
        return artifacts.get(name.toLowerCase());
    }

    public String name() {
        return name;
    }

    public Integer getKeyPattern() {
        return keyPattern;
    }

    public abstract void use(Place currentPlace, Character character);

    @Override
    public String toString() {
        return "Artifact{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", value='" + value + '\'' +
                ", mobility=" + mobility +
                ", keyPattern=" + keyPattern +
                '}';
    }
}
