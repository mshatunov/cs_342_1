package com.kazi.game.model.artifact;

import com.kazi.game.io.IO;
import com.kazi.game.model.character.Character;
import com.kazi.game.model.place.Place;

import java.util.Scanner;

public class ToolArtifact extends Artifact {
    public ToolArtifact(Scanner scanner) {
        super(scanner);
    }

    @Override
    public void use(Place currentPlace, Character character) {
        IO.getCurrentInterface().display("We are using tool " + this.name());
    }
}
