package com.kazi.game.model.artifact;

import java.util.stream.Stream;

public enum ArtifactType {
    BASIC("BASIC"),
    POTION("POTION"),
    KEY("KEY"),
    TOOL("TOOL");

    ArtifactType(String type) {
        this.type = type;
    }

    private String type;

    public String getType() {
        return type;
    }

    public static ArtifactType getArtifactType(String inputType) {
        return Stream.of(ArtifactType.values())
                .filter(t -> t.getType().equalsIgnoreCase(inputType))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No such artifact type"));

    }
}
