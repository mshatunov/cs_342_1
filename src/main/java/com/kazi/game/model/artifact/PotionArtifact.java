package com.kazi.game.model.artifact;

import com.kazi.game.io.IO;
import com.kazi.game.model.character.Character;
import com.kazi.game.model.place.Place;

import java.util.Scanner;

public class PotionArtifact extends Artifact {
    public PotionArtifact(Scanner scanner) {
        super(scanner);
    }

    @Override
    public void use(Place currentPlace, Character character) {
        IO.getCurrentInterface().display("Using potion " + this.name());
        int newHealth = character.getHealth() + 10;
        IO.getCurrentInterface().display("Increasing health. Now = " + newHealth);
        character.setHealth(newHealth);
    }
}
