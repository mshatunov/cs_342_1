package com.kazi.game.model.artifact;

import java.util.Scanner;

public class ArtifactFactory {
    public static Artifact createArtifact(Scanner scanner, String artifactType) {
        switch (ArtifactType.getArtifactType(artifactType)) {
            case BASIC:
                return new BasicArtifact(scanner);
            case POTION:
                return new PotionArtifact(scanner);
            case KEY:
                return new KeyArtifact(scanner);
            case TOOL:
                return new ToolArtifact(scanner);
            default:
                throw new IllegalStateException("No such place type");
        }
    }
}
