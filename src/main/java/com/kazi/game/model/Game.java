package com.kazi.game.model;

import com.kazi.game.io.IO;
import com.kazi.game.io.UserInterface;
import com.kazi.game.model.character.Character;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.stream.Collectors;

public class Game {

    private final String player = System.getProperty("user.name");
    private final String name;

    private UserInterface io;

    public Game(String name) {
        changeIO();

        this.name = name;
        io.display("Hello, " + player);
        try {
            io.display(player + "'s ip - " + InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public UserInterface getIo() {
        return io;
    }

    private void changeIO() {
        IO.getCurrentInterface().display("Select user interface:");
        IO.getCurrentInterface().display(IO.TEXT + " - Text interface");
        int interfaceID;
        try {
            interfaceID = Integer.parseInt(IO.getCurrentInterface().getLine());
            IO.selectInterface(interfaceID);
        } catch (Exception e) {
            IO.selectInterface(IO.TEXT);
        }
        io = IO.getCurrentInterface();
    }

    public void play() {
        io.display("We are starting to play a game " + this.name);
        printActions();
        List<Character> players = Character.characters.values().stream()
                .filter(Character::isPlayer)
                .collect(Collectors.toList());
        List<Character> npcs = Character.characters.values().stream()
                .filter(c -> !c.isPlayer())
                .collect(Collectors.toList());
        while (true) {
            for (Character character : players) {
                io.display("Current place is: " + character.getCurrentPlace().name());
                io.display("Your next move, " + character.getName());
                String input = io.getLine();
                if (!character.makeMove(input))
                    System.exit(0);
                for (Character npc : npcs) {
                    if (npc.getCurrentPlace().equals(character.getCurrentPlace())) {
                        io.display("NPC's turn - " + npc.getName());
                        npc.makeMove("TALK");
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Game{" +
                "player='" + player + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    private void printActions() {
        io.display("You can do these actions");
        io.display("LOOK - print current info");
        io.display("MOVE N/W/U/D/... - move to another location");
        io.display("QUIT/EXIT - exit game");
        io.display("INVENTORY/INVE - print out current artifacts");
        io.display("GET artifactName - get artifactName from current location");
        io.display("DROP artifactName - drop artifactName from inventory");
        io.display("USE artifactName - use artifactName from inventory to lock/unlock location");
        io.display("WAVE - wave hand");
        io.display("TEXT - switch to text ui");
        io.display("GUI # - switch to # ui. 0 - is text UI");
    }

}
