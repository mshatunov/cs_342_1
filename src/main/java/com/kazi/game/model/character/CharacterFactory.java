package com.kazi.game.model.character;

import com.kazi.game.model.character.npc.*;

import java.util.Scanner;

public class CharacterFactory {
    public static Character createCharacter(Scanner scanner) {
        switch (CharacterType.getCharacterType(scanner.next())) {
            case PLAYER:
                return new PlayerCharacter(scanner);
            case NPC:
                return new NPCCharacter(scanner);
            case NPC_FRIENDLY:
                return new FriendlyNPCCharacter(scanner);
            case NPC_AGGRESSIVE:
                return new AggressiveNPCCharacter(scanner);
            case NPC_CHAOTIC:
                return new ChaoticNPCCharacter(scanner);
            case NPC_HELPFUL:
                return new HelpfulNPCCharacter(scanner);
            case NPC_MISCHIEVOUS:
                return new MischievousNPCCharacter(scanner);
            default:
                throw new IllegalStateException("No such character type");
        }
    }
}
