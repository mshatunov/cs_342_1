package com.kazi.game.model.character;

import com.kazi.game.decision.UIDecisionMaker;
import com.kazi.game.io.IO;
import com.kazi.game.model.Move;

import java.util.Scanner;

public class PlayerCharacter extends Character {

    public PlayerCharacter(Scanner scanner) {
        super(scanner);
        this.decisionMaker = new UIDecisionMaker();
        IO.getCurrentInterface().display("Created character:" + this);
    }

    public boolean makeMove(String input) {
        Move move = getMove(input);
        switch (move.getType()) {
            case LOOK:
                this.display();
                break;
            case QUIT:
                IO.getCurrentInterface().display("Goodbye, " + this.getName());
                System.exit(0);
                break;
            case INVENTORY:
                IO.getCurrentInterface().display("You have these artifacts:");
                artifacts.forEach(System.out::println);
                break;
            case ARTIFACT_ADD:
                addArtifact(move.getArgument());
                break;
            case ARTIFACT_DROP:
                dropArtifact(move.getArgument());
                break;
            case ARTIFACT_USE:
                useArtifact(move);
                break;
            case MOVE:
                return goToNextPlace(move);
            case WAVE_HAND:
                IO.getCurrentInterface().display("Hellooooooooooo! =)");
                break;
            case TEXT:
                IO.selectInterface(IO.TEXT);
                IO.getCurrentInterface().display("Interface switched to text");
                break;
            case GUI:
                int interfaceID;
                try {
                    interfaceID = Integer.parseInt(IO.getCurrentInterface().getLine());
                } catch (Exception e) {
                    interfaceID = IO.TEXT;
                }
                IO.selectInterface(interfaceID);
                IO.getCurrentInterface().display("Interface switched to " + interfaceID);
                break;
            default:
                IO.getCurrentInterface().display("No such move");
        }
        return true;
    }
}
