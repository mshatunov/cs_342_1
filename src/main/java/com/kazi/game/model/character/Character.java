package com.kazi.game.model.character;

import com.kazi.game.decision.DecisionMaker;
import com.kazi.game.decision.UIDecisionMaker;
import com.kazi.game.io.IO;
import com.kazi.game.model.Move;
import com.kazi.game.model.artifact.Artifact;
import com.kazi.game.model.place.Place;

import java.util.*;

public abstract class Character {
    public static final Map<Integer, Character> characters = new HashMap<>();
    protected int id;
    protected String name;
    protected String description;
    protected DecisionMaker decisionMaker;
    protected Place currentPlace;
    protected int health = 100;

    protected List<Artifact> artifacts = new ArrayList<>();

    public Character(Scanner scanner) {
        int placeId = scanner.nextInt();
        this.currentPlace = Place.getPlaceById(placeId);

        this.id = scanner.nextInt();
        this.name = scanner.next();

        int linesOfDescription = scanner.nextInt();
        scanner.nextLine();

        String description = "";
        for (int i = 0; i < linesOfDescription; i++) {
            description += scanner.nextLine();
        }
        this.description = description;
        characters.put(id, this);
    }

    public static Character getCharacterByID(int id) {
        return characters.get(id);
    }

    public String getName() {
        return name;
    }

    public Place getCurrentPlace() {
        return currentPlace;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public List<Artifact> getArtifacts() {
        return artifacts;
    }

    public void print() {
        IO.getCurrentInterface().display(this.toString());
    }

    public void display() {
        IO.getCurrentInterface().display("You're in " + currentPlace.name());
        IO.getCurrentInterface().display("It has artifacts " + currentPlace.getArtifacts());
        IO.getCurrentInterface().display("Available directions " + currentPlace.getDirections());
        IO.getCurrentInterface().display("Your name - " + name + ", you have these artifacts" + artifacts);
    }

    public abstract boolean makeMove(String input);

    public boolean isPlayer() {
        return this.decisionMaker.getClass().equals(UIDecisionMaker.class);
    }

    protected boolean goToNextPlace(Move move) {
        Place place = currentPlace.followDirection(move.getArgument());
        currentPlace = place;
        if (currentPlace.name().equals("Exit")) {
            IO.getCurrentInterface().display("You win!");
            return false;
        }

        if (currentPlace.name().equals("Game over")) {
            IO.getCurrentInterface().display("You lost =(");
            return false;
        }
        return true;
    }

    protected void addArtifact(String artifactName) {
        Iterator<Artifact> artifactIterator = currentPlace.getArtifacts().iterator();
        while (artifactIterator.hasNext()) {
            Artifact artifact = artifactIterator.next();
            if (artifact.name().equalsIgnoreCase(artifactName)) {
                artifacts.add(artifact);
                artifactIterator.remove();
                IO.getCurrentInterface().display("Added artifact " + artifact.name());
                break;
            }
        }
    }

    protected void dropArtifact(String artifactName) {
        Iterator<Artifact> artifactIterator = artifacts.iterator();
        while (artifactIterator.hasNext()) {
            Artifact artifact = artifactIterator.next();
            if (artifact.name().equalsIgnoreCase(artifactName)) {
                artifactIterator.remove();
                currentPlace.addArtifact(artifact);
                IO.getCurrentInterface().display("Dropped artifact " + artifactName);
            }
        }
    }

    protected Move getMove(String input) {
        return decisionMaker.getMove(input);
    }

    protected void useArtifact(Move move) {
        Artifact artifact = Artifact.getArtifactByName(move.getArgument());
        if (artifact != null) {
            artifact.use(currentPlace, this);
            artifacts.remove(artifact);
        } else {
            IO.getCurrentInterface().display("There is no artifact " + move.getArgument());
        }

    }

    @Override
    public String toString() {
        return "Character{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", decisionMaker=" + decisionMaker +
                ", currentPlace=" + currentPlace +
                ", artifacts=" + artifacts +
                '}';
    }
}
