package com.kazi.game.model.character.npc;

import com.kazi.game.decision.AIDecisionMaker;
import com.kazi.game.io.IO;
import com.kazi.game.model.artifact.Artifact;
import com.kazi.game.model.Move;
import com.kazi.game.model.character.Character;

import java.util.Scanner;

public class NPCCharacter extends Character {

    public NPCCharacter(Scanner scanner) {
        super(scanner);
        this.decisionMaker = new AIDecisionMaker();
        IO.getCurrentInterface().display("Created character:" + this);
    }

    public boolean makeMove(String input) {
        Move move = getMove(input);
        switch (move.getType()) {
            case ARTIFACT_ADD:
                addArtifact(move.getArgument());
                break;
            case ARTIFACT_DROP:
                dropArtifact(move.getArgument());
                break;
            case ARTIFACT_USE:
                useArtifact(move);
                break;
            case MOVE:
                return goToNextPlace(move);
            default:
                IO.getCurrentInterface().display("No such move");
        }
        return true;
    }
}
