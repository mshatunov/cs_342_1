package com.kazi.game.model.character.npc;

import com.kazi.game.io.IO;
import com.kazi.game.model.Move;

import java.util.Scanner;

public class HelpfulNPCCharacter extends NPCCharacter {
    public HelpfulNPCCharacter(Scanner scanner) {
        super(scanner);
    }

    @Override
    public boolean makeMove(String input) {
        Move move = getMove(input);
        switch (move.getType()) {
            case TALK:
                IO.getCurrentInterface().display("How can I help you?");
                break;
            default:
                return super.makeMove(input);
        }
        return true;
    }
}
