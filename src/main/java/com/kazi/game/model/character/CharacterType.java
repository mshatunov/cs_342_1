package com.kazi.game.model.character;

import java.util.stream.Stream;

public enum CharacterType {
    PLAYER("PLAYER"),
    NPC("NPC"),
    NPC_FRIENDLY("NPC_FRIENDLY"),
    NPC_AGGRESSIVE("NPC_AGGRESSIVE"),
    NPC_CHAOTIC("NPC_CHAOTIC"),
    NPC_HELPFUL("NPC_HELPFUL"),
    NPC_MISCHIEVOUS("NPC_MISCHIEVOUS");

    CharacterType(String type) {
        this.type = type;
    }

    private String type;

    public String getType() {
        return type;
    }

    public static CharacterType getCharacterType(String inputType) {
        return Stream.of(CharacterType.values())
                .filter(t -> t.getType().equalsIgnoreCase(inputType))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No such character type"));

    }
}
