package com.kazi.game.model;

public class Move {

    private MoveType type;
    private String argument;

    public MoveType getType() {
        return type;
    }

    public String getArgument() {
        return argument;
    }

    public Move(MoveType type) {
        this.type = type;
    }

    public Move(MoveType type, String argument) {
        this.type = type;
        this.argument = argument;
    }

    public enum MoveType {
        LOOK,
        QUIT,
        INVENTORY,
        ARTIFACT_ADD,
        ARTIFACT_DROP,
        ARTIFACT_USE,
        MOVE,
        TALK,
        WAVE_HAND,
        TEXT,
        GUI,
        NONE
    }
}
