package com.kazi.game.model;

import com.kazi.game.model.artifact.Artifact;
import com.kazi.game.model.place.Place;

import java.util.Scanner;
import java.util.stream.Stream;

public class Direction {

    private final int id;
    private final Place from;
    private final Place to;
    private final DirType dir;
    private boolean isLocked;
    private int lockPattern;

    public Direction(Scanner scanner) {
        int id = scanner.nextInt();
        int fromId = scanner.nextInt();
        DirType dir = DirType.valueOf(scanner.next());
        int toId = scanner.nextInt();
        int lockPattern = scanner.nextInt();

        this.id = id;
        this.from = Place.getPlaceById(fromId);
        this.to = Place.getPlaceById(toId);
        this.dir = dir;
        this.isLocked = false;
        this.lockPattern = lockPattern;

        Place.getPlaceById(fromId).addDirection(this);

        System.out.println("Created direction:" + this);
    }

    public static DirType valueOfDirType(String direction) {
        return Stream.of(DirType.values())
                .filter(d -> d.getAbbreviation().equals(direction))
                .findFirst()
                .orElse(DirType.NONE);
    }

    public DirType getDir() {
        return dir;
    }

    public boolean match(String s) {
        return Stream.of(DirType.values())
                .anyMatch(d -> d.match(s));
    }

    public void lock() {
        this.isLocked = true;
    }

    public void unlock() {
        this.isLocked = false;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public Place follow() {
        return isLocked ? from : to;
    }

    public void print() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "Direction{" +
                ", from=" + from.name() +
                ", to=" + to.name() +
                ", dir=" + dir.getAbbreviation() +
                ", isLocked=" + isLocked +
                ", lockPattern=" + lockPattern +
                '}';
    }

    public void useKey(Artifact artifact) {
        System.out.println("Using " + artifact.name());
        if (artifact.getKeyPattern() > 0 && artifact.getKeyPattern() == lockPattern) {
            isLocked = !isLocked;
        }
    }

    public enum DirType {
        NONE("None", "None"),
        N("North", "N"),
        S("South", "S"),
        E("East", "E"),
        W("West", "W"),
        U("Up", "U"),
        D("Down", "D"),
        NE("North east", "NE"),
        NW("North west", "NW"),
        SE("South east", "SE"),
        SW("South west", "SW"),
        NNE("North northeast", "NNE"),
        NNW("North northwest", "NNW"),
        ENE("East northeast", "ENE"),
        WNW("West northwest", "WNW"),
        ESE("East southeast", "ESE"),
        WSW("West southwest", "WSW"),
        SSE("South southeast", "SSE"),
        SSW("South southwest", "SSW");

        private String text;
        private String abbreviation;

        DirType(String text, String abbreviation) {
            this.text = text;
            this.abbreviation = abbreviation;
        }

        public String getText() {
            return text;
        }

        public String getAbbreviation() {
            return abbreviation;
        }

        public boolean match(String s) {
            return this.getAbbreviation().equalsIgnoreCase(s);
        }

        @Override
        public String toString() {
            return "DirType{" +
                    "text='" + text + '\'' +
                    '}';
        }
    }

}
