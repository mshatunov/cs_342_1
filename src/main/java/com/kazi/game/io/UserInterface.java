package com.kazi.game.io;

public interface UserInterface {
    void display(String s);

    void display(int i);

    String getLine();
}
