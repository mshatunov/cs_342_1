package com.kazi.game.io;

public class IO implements UserInterface{
    public static final int TEXT = 0;

    //initial interface is text ui
    private static UserInterface currentInterface = new TextUserInterface();

    @Override
    public void display(String s) {
        currentInterface.display(s);
    }

    @Override
    public void display(int i) {
        this.display(String.valueOf(i));
    }

    @Override
    public String getLine() {
        return currentInterface.getLine();
    }

    public static void selectInterface(int interfaceID) {
        if (interfaceID == TEXT) {
            currentInterface = new TextUserInterface();
        } else {
            throw new RuntimeException("No such user interface");
        }
    }

    public static UserInterface getCurrentInterface() {
        return currentInterface;
    }
}
