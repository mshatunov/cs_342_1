package com.kazi.game.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TextUserInterface implements UserInterface {

    private final BufferedReader br;

    public TextUserInterface() {
        try {
            br = new BufferedReader(new InputStreamReader(System.in));
        } catch (Exception e) {
            throw new RuntimeException("Error initialising text UI");
        }
    }

    @Override
    public void display(String s) {
        System.out.println(s);
    }

    @Override
    public void display(int i) {
        this.display(String.valueOf(i));
    }

    @Override
    public String getLine() {
        try {
            return br.readLine().toUpperCase();
        } catch (Exception e) {
            this.display("Error in game");
            e.printStackTrace();
            return "";
        }
    }
}
