# CS 342 – Software Design – Fall 2018

## Term Project Part I
Development of Game, Place, and Direction Classes

Run with GDF file settings 

```
./gradlew clean build && java -classpath build/classes/java/main com.kazi.game.GameTester
```

or you can specify path to GDF file
```
./gradlew clean build && java -classpath build/classes/java/main com.kazi.game.GameTester src/main/resources/game.gdf
```

Run using java command

```
./gradlew clean build && java -classpath build/classes/java/main com.kazi.game.PredefinedGameTester 
```

or using gradle

```
./gradlew clean run
```
### Custom characters
Supported in gdf version > 5.0.

Available types:
- PLAYER
- NPC
- NPC_FRIENDLY
- NPC_AGGRESSIVE
- NPC_CHAOTIC
- NPC_HELPFUL
- NPC_MISCHIEVOUS

*NPCs are talking in the current place. Different phrases are available for each NPC*

must be specified on the first line.
Example:
``` 
NPC_FRIENDLY
2
2 Friendly_npc
1
Test npc Friendly
```

In 5.0 only PLAYER and NPC are supported

### Custom places
Supported in gdf version > 5.0. Default is `BASIC` for version <= 5.0 

Available types:
```
- BASIC : no additional behaviour
- MAGICAL : all directions are unlocked in such places
- DARK : player can't see any artifacts in such places
- MAGNETIC : player can't add any artifact in that room
```
must be specified on the first line.

Example:
``` 
MAGNETIC 4 Potions Lab
1
Potions Lab description
```

### Custom artifacts
Supported in gdf version > 5.0. Default is `BASIC` for version <= 5.0 

Available types:
```
- BASIC : no additional behaviour
- POTION : increments player's health by 10
- KEY : unlocks directions
- TOOL : custom message for using tools
```
must be specified on the first line.

Example:
``` 
POTION -1
3 health_potion 5 6 health
1
Health potion
```

### Custom moves
        LOOK,
        QUIT,
        INVENTORY,
        ARTIFACT_ADD,
        ARTIFACT_DROP,
        ARTIFACT_USE,
        MOVE,
        TALK,
        WAVE_HAND,
        TEXT,
        GUI #,
        NONE